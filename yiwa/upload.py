# coding: utf-8

"""上传文件，通用方法"""

import os
import uuid
from werkzeug.datastructures import FileStorage
from yiwa.settings import BASE_DIR


class FileUploader(object):
    """文件上传器"""

    def image(self, dir_path: str, image_file: FileStorage):
        """上传图片"""
        dir_abs_path = os.path.join(BASE_DIR, "apps", dir_path)
        if not os.path.exists(dir_abs_path):
            return False
        file_ext = image_file.mimetype.split("/")[-1]
        image_new_name = f"{str(uuid.uuid1())}.{file_ext}"
        try:
            image_file.save(os.path.join(dir_abs_path, image_new_name))
            return image_new_name
        except:
            return False
