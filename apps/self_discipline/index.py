# coding: utf8

"""页面"""

from apps import app
from flask import render_template
import datetime
from collections import OrderedDict
from apps.self_discipline.db import s3db

num_week = 7
weeks = {
    0: "星期一",
    1: "星期二",
    2: "星期三",
    3: "星期四",
    4: "星期五",
    5: "星期六",
    6: "星期天",
}


def _get_days():
    """7天内容"""
    num_now = datetime.datetime.now().weekday()
    sql = f"""select createtime, option, scores.score, scores.image_path, `date`, week 
        from day 
        left join scores on scores.id = day.score
        where `date` >= date("now", "-{num_now} day")
        order by `date`, option;
        """
    listDays = s3db.select(sql)

    dictDays = OrderedDict()
    # 初始化
    for num, week in weeks.items():
        num_delta = num - num_now
        today = True if num_delta == 0 else False
        daytime = datetime.date.today() + datetime.timedelta(days=num_delta)
        dictDays[week] = {"today": today,
                          "date": daytime.strftime('%m-%d'),
                          "options": {}}
    # 组装
    for (createtime, option, score, image_path, date, week) in listDays:
        try:
            _day = dictDays[weeks[week]]
            createtime = datetime.datetime.strptime(createtime, "%Y-%m-%d %H:%M:%S")
            _day["options"].update({option: {
                "createtime": createtime.strftime('%H:%M'),
                "score": score,
                "image_path": image_path
            }})
        except Exception as e:
            print("key值不存在", e)
    return dictDays


def _get_options():
    """所有鼓励项"""
    sql = """select id, `name`, image_path 
        from options 
        where enable = 1
        order by `order`;"""
    return s3db.select(sql)

def _get_awards():
    """奖励"""
    sql = """select `name`, score, image_path
        from awards
        where got = 0
        order by `order`;
        """
    return s3db.select(sql)

def _get_balance():
    """余额"""
    sql = """select balance
        from exchange_records
        where last=1
        limit 1;
    """
    res = s3db.select_one(sql)
    return res.get("balance", 0)


@app.route("/self_discipline/week")
def week():
    options = _get_options()
    days = _get_days()
    awards = _get_awards()
    balance = _get_balance()
    return render_template("self_discipline/week.html", **locals())
